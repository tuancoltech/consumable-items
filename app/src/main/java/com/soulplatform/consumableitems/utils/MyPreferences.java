package com.soulplatform.consumableitems.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class MyPreferences {

    private static Context sContext;

    private static final String PREF_KEY_SESSION_TOKEN = "pref_key_session_token";
    private static final String PREF_KEY_USER_ID = "pref_key_user_id";

    public static void init(@NonNull Context context) {
        sContext = Preconditions.checkNotNull(context);
    }

    public static void saveSessionToken(@NonNull String sessionToken) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(sContext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_KEY_SESSION_TOKEN, sessionToken);
        editor.apply();
    }

    public static String getSessionToken() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(sContext);
        return preferences.getString(PREF_KEY_SESSION_TOKEN, "");
    }

    public static void saveUserId(@NonNull String userId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(sContext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_KEY_USER_ID, userId);
        editor.apply();
    }

    public static String getUserId() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(sContext);
        return preferences.getString(PREF_KEY_USER_ID, "");
    }

}
