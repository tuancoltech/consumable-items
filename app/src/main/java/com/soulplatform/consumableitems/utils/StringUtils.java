package com.soulplatform.consumableitems.utils;

import android.text.TextUtils;

import java.nio.charset.Charset;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.RequestBody;
import okio.Buffer;
import okio.ByteString;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class StringUtils {

    public static String hmacSha256(String key, String value) {
        return hmacSha(key, value, "HmacSHA256");
    }

    private static String hmacSha(String key, String value, String shaType) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes("UTF-8"), shaType);
            Mac mac = Mac.getInstance(shaType);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(value.getBytes("UTF-8"));

            byte[] hexArray = {
                    (byte)'0', (byte)'1', (byte)'2', (byte)'3',
                    (byte)'4', (byte)'5', (byte)'6', (byte)'7',
                    (byte)'8', (byte)'9', (byte)'a', (byte)'b',
                    (byte)'c', (byte)'d', (byte)'e', (byte)'f'
            };
            byte[] hexChars = new byte[rawHmac.length * 2];
            for ( int j = 0; j < rawHmac.length; j++ ) {
                int v = rawHmac[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static String getConcatString(String... params) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < params.length; i ++) {
            if (!TextUtils.isEmpty(params[i])) {
                if (i < (params.length - 1)) {
                    stringBuilder.append(params[i]).append("+");
                } else {
                    stringBuilder.append(params[i]);
                }
            }
        }
        return stringBuilder.toString();
    }

    public static String hmacSha256Hex(String sessionToken, String method, String completePath,
                                 String requestBodyString, String unixTime) {
        try {
            final Charset ascii = Charset.forName("ASCII");
            final Buffer buffer = new Buffer().writeString(method, ascii)
                    .writeString("+", ascii)
                    .writeString(completePath, ascii)
                    .writeString("+", ascii);

            if (!TextUtils.isEmpty(requestBodyString)) {
                buffer.writeString(requestBodyString, Charset.forName("UTF-8"));
            }

            return buffer.writeString("+", ascii)
                    .writeString(unixTime, ascii)
                    .hmacSha256(ByteString.encodeUtf8(sessionToken))
                    .hex();
        } catch (Exception throwable) {
            throwable.printStackTrace();
            return "";
        }
    }

}
