package com.soulplatform.consumableitems.utils;

/**
 * Created by Tuan Nguyen on 12/17/2017.
 */

public class Const {

    public static final String API_VERSION = "1.0.0";
    public static final String MEDIA_TYPE_JSON = "application/json; charset=utf-8";

    public static final int ERR_CODE_UNKNOWN = -1;

}
