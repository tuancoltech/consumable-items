package com.soulplatform.consumableitems;

import android.app.Application;

import com.soulplatform.consumableitems.utils.MyPreferences;

public class MyApplication extends Application {

    private static final String PREF_FILE_NAME = "my_preferences.xml";

    @Override
    public void onCreate() {
        super.onCreate();
        MyPreferences.init(getApplicationContext());

    }
}
