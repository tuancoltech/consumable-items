package com.soulplatform.consumableitems.data.repository.authentication.source;

import android.support.annotation.NonNull;
import android.util.Log;

import com.soulplatform.consumableitems.data.repository.authentication.AuthenDataSource;
import com.soulplatform.consumableitems.data.repository.authentication.source.remote
        .AuthenRemoteDataSource;
import com.soulplatform.consumableitems.domain.model.authentication.login.ErrorModel;
import com.soulplatform.consumableitems.domain.model.authentication.login.LoginResponseModel;
import com.soulplatform.consumableitems.utils.Preconditions;

import java.io.IOException;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class AuthenRepository implements AuthenDataSource {

    private static AuthenRepository sAuthenRepository = null;

    private final AuthenDataSource mAuthenLocalDataSource;

    private final AuthenDataSource mAuthenRemoteDataSource;

    private AuthenRepository(@NonNull AuthenDataSource authenLocalDataSource,
                             @NonNull AuthenDataSource authenRemoteDataSource) {

        mAuthenLocalDataSource = Preconditions.checkNotNull(authenLocalDataSource);
        mAuthenRemoteDataSource = Preconditions.checkNotNull(authenRemoteDataSource);
    }

    public static AuthenRepository getInstance(@NonNull AuthenDataSource authenLocalDataSource,
                                        @NonNull AuthenDataSource authenRemoteDataSource) {
        if (sAuthenRepository == null) {
            synchronized (AuthenRepository.class) {
                if (sAuthenRepository == null) {
                    sAuthenRepository = new AuthenRepository(authenLocalDataSource,
                            authenRemoteDataSource);
                }
            }
        }
        return sAuthenRepository;
    }

    @Override
    public void login(@NonNull String userName, @NonNull String password, @NonNull String apiKey,
                      @NonNull String apiVersion, @NonNull final LoginCallback loginCallback) {

        mAuthenRemoteDataSource.login(userName, password, apiKey, apiVersion,
                new LoginCallback() {
                    @Override
                    public void onLoginSuccess(@NonNull LoginResponseModel loginResponseModel) {
                        loginCallback.onLoginSuccess(loginResponseModel);
                    }

                    @Override
                    public void onLoginFail(@NonNull String errorModel) {
                        Log.i("tuancoltech", " response error msg 2: " + errorModel);
                        loginCallback.onLoginFail(errorModel);
                    }
                });
    }
}
