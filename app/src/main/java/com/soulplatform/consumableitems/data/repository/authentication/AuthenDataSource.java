package com.soulplatform.consumableitems.data.repository.authentication;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.domain.model.authentication.login.ErrorModel;
import com.soulplatform.consumableitems.domain.model.authentication.login.LoginResponseModel;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public interface AuthenDataSource {

    void login(@NonNull String userName,
               @NonNull String password,
               @NonNull String apiKey,
               @NonNull String apiVersion,
               @NonNull LoginCallback loginCallback);

    interface LoginCallback {

        void onLoginSuccess(@NonNull LoginResponseModel loginResponseModel);

        void onLoginFail(@NonNull String errorModel);

    }

}
