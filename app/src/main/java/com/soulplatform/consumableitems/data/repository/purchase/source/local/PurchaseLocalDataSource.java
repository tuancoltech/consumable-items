package com.soulplatform.consumableitems.data.repository.purchase.source.local;

import android.content.Context;
import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.data.repository.purchase.PurchaseDataSource;
import com.soulplatform.consumableitems.utils.Preconditions;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class PurchaseLocalDataSource implements PurchaseDataSource {

    private final Context mContext;
    private static PurchaseLocalDataSource sPurchaseLocalDataSource = null;

    private PurchaseLocalDataSource(@NonNull Context context) {
        mContext = Preconditions.checkNotNull(context);
    }

    public static PurchaseLocalDataSource getInstance(@NonNull Context context) {
        if (sPurchaseLocalDataSource == null) {
            synchronized (PurchaseLocalDataSource.class) {
                if (sPurchaseLocalDataSource == null) {
                    sPurchaseLocalDataSource = new PurchaseLocalDataSource(context);
                }
            }
        }
        return sPurchaseLocalDataSource;
    }

    @Override
    public void getAllBundles(@NonNull String apiVersion, @NonNull GetAllBundleCallback
            getAllBundleCallback) {

    }

    @Override
    public void consumeItem(@NonNull String itemId, int quantity, @NonNull String apiVersion,
                            @NonNull ConsumeItemCallback consumeItemCallback) {

    }

    @Override
    public void validateOrder(@NonNull String token, @NonNull String subscriptionId, @NonNull
            String signature, @NonNull String apiVersion, @NonNull ValidateOrderCallback
                                      validateOrderCallback) {

    }

    @Override
    public void getMyPurchases(@NonNull String apiVersion, @NonNull GetMyPurchaseCallback
            getMyPurchaseCallback) {

    }
}
