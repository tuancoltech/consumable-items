package com.soulplatform.consumableitems.data.repository.purchase;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.domain.model.purchase.bundle.GetAllBundleResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeItemResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.GetMyPurchaseResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderResponse;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public interface PurchaseDataSource {

    void getAllBundles(@NonNull String apiVersion,
                       @NonNull GetAllBundleCallback getAllBundleCallback);

    void consumeItem(@NonNull String itemId,
                     int quantity,
                     @NonNull String apiVersion,
                     @NonNull ConsumeItemCallback consumeItemCallback);

    void validateOrder(@NonNull String token,
                       @NonNull String subscriptionId,
                       @NonNull String signature,
                       @NonNull String apiVersion,
                       @NonNull ValidateOrderCallback validateOrderCallback);

    void getMyPurchases(@NonNull String apiVersion,
                        @NonNull GetMyPurchaseCallback getMyPurchaseCallback);

    interface GetAllBundleCallback {

        void onGetAllBundleSuccess(@NonNull GetAllBundleResponse getAllBundleResponse);

        void onGetAllBundleFail(@NonNull String errorModel);

    }

    interface ConsumeItemCallback {

        void onConsumeItemSuccess(@NonNull ConsumeItemResponse consumeItemResponse);

        void onConsumeItemFail(@NonNull String errorModel);

    }

    interface ValidateOrderCallback {

        void onValidateOrderSuccess(@NonNull ValidateOrderResponse validateOrderResponse);

        void onValidateOrderFail(@NonNull String errorModel);

    }

    interface GetMyPurchaseCallback {

        void onGetMyPurchaseSuccess(@NonNull GetMyPurchaseResponse getMyPurchaseResponse);

        void onGetMyPurchaseFail(@NonNull String errorModel);

    }

}
