package com.soulplatform.consumableitems.data.repository.authentication.source.remote;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.soulplatform.consumableitems.data.network.ApiManager;
import com.soulplatform.consumableitems.data.repository.authentication.AuthenDataSource;
import com.soulplatform.consumableitems.domain.model.authentication.login.ErrorModel;
import com.soulplatform.consumableitems.domain.model.authentication.login.LoginResponseModel;
import com.soulplatform.consumableitems.utils.Preconditions;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class AuthenRemoteDataSource implements AuthenDataSource {

    private static AuthenRemoteDataSource sAuthenRemoteDataSource = null;
    private final Context mContext;

    private AuthenRemoteDataSource(@NonNull Context context) {
        mContext = Preconditions.checkNotNull(context);
    }

    public static AuthenRemoteDataSource getInstance(@NonNull Context context) {
        if (sAuthenRemoteDataSource == null) {
            synchronized (AuthenRemoteDataSource.class) {
                if (sAuthenRemoteDataSource == null) {
                    sAuthenRemoteDataSource = new AuthenRemoteDataSource(context);
                }
            }
        }
        return sAuthenRemoteDataSource;
    }


    @Override
    public void login(@NonNull String userName, @NonNull String password, @NonNull String apiKey,
                      @NonNull String apiVersion, @NonNull final LoginCallback loginCallback) {
        ApiManager.login(mContext, userName, password, apiKey, apiVersion,
                new Callback<LoginResponseModel>() {
                    @Override
                    public void onResponse(Call<LoginResponseModel> call,
                                           Response<LoginResponseModel> response) {
                        if (response != null && response.isSuccessful()
                                && response.body() != null &&
                                response.body().getAuthorizationModel() != null) {

                            loginCallback.onLoginSuccess(response.body());
                        } else {
//                            Gson gson = new Gson();
//                            ErrorModel errorModel = gson.fromJson(response.errorBody().charStream(),
//                                    ErrorModel.class);

                            try {
                                loginCallback.onLoginFail(response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                        t.printStackTrace();
                        Log.i("tuancoltech", "onFailure 1");
                        loginCallback.onLoginFail(t.toString());
                    }
                });
    }
}
