package com.soulplatform.consumableitems.data.repository.purchase.source;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.data.repository.purchase.PurchaseDataSource;
import com.soulplatform.consumableitems.domain.model.purchase.bundle.GetAllBundleResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeItemResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.GetMyPurchaseResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderResponse;
import com.soulplatform.consumableitems.utils.Preconditions;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class PurchaseRepository implements PurchaseDataSource {

    private static PurchaseRepository sPurchaseRepository = null;
    private final PurchaseDataSource mPurchaseLocalDataSource;
    private final PurchaseDataSource mPurchaseRemoteDataSource;

    private PurchaseRepository(@NonNull PurchaseDataSource purchaseLocalDataSource,
                               @NonNull PurchaseDataSource purchaseRemoteDataSource) {

        mPurchaseLocalDataSource = Preconditions.checkNotNull(purchaseLocalDataSource);
        mPurchaseRemoteDataSource = Preconditions.checkNotNull(purchaseRemoteDataSource);
    }

    public static PurchaseRepository getInstance(@NonNull PurchaseDataSource
                                                         purchaseLocalDataSource,
                                                 @NonNull PurchaseDataSource
                                                         purchaseRemoteDataSource) {

        if (sPurchaseRepository == null) {
            synchronized (PurchaseRepository.class) {
                if (sPurchaseRepository == null) {
                    sPurchaseRepository = new PurchaseRepository(purchaseLocalDataSource,
                            purchaseRemoteDataSource);
                }
            }
        }
        return sPurchaseRepository;
    }

    @Override
    public void getAllBundles(@NonNull String apiVersion, @NonNull final GetAllBundleCallback
            getAllBundleCallback) {

        mPurchaseRemoteDataSource.getAllBundles(apiVersion, new GetAllBundleCallback() {
            @Override
            public void onGetAllBundleSuccess(@NonNull GetAllBundleResponse getAllBundleResponse) {
                getAllBundleCallback.onGetAllBundleSuccess(getAllBundleResponse);
            }

            @Override
            public void onGetAllBundleFail(@NonNull String errorModel) {
                getAllBundleCallback.onGetAllBundleFail(errorModel);
            }
        });
    }

    @Override
    public void consumeItem(@NonNull String itemId, int quantity, @NonNull String apiVersion,
                            @NonNull final ConsumeItemCallback consumeItemCallback) {

        mPurchaseRemoteDataSource.consumeItem(itemId, quantity, apiVersion,
                new ConsumeItemCallback() {
                    @Override
                    public void onConsumeItemSuccess(@NonNull ConsumeItemResponse
                                                             consumeItemResponse) {
                        consumeItemCallback.onConsumeItemSuccess(consumeItemResponse);
                    }

                    @Override
                    public void onConsumeItemFail(@NonNull String errorModel) {
                        consumeItemCallback.onConsumeItemFail(errorModel);
                    }
                });
    }

    @Override
    public void validateOrder(@NonNull String token, @NonNull String subscriptionId,
                              @NonNull String signature, @NonNull String apiVersion,
                              @NonNull final ValidateOrderCallback validateOrderCallback) {

        mPurchaseRemoteDataSource.validateOrder(token, subscriptionId, signature, apiVersion,
                new ValidateOrderCallback() {
                    @Override
                    public void onValidateOrderSuccess(@NonNull ValidateOrderResponse
                                                               validateOrderResponse) {
                        validateOrderCallback.onValidateOrderSuccess(validateOrderResponse);
                    }

                    @Override
                    public void onValidateOrderFail(@NonNull String errorModel) {
                        validateOrderCallback.onValidateOrderFail(errorModel);
                    }
                });
    }

    @Override
    public void getMyPurchases(@NonNull String apiVersion,
                               @NonNull final GetMyPurchaseCallback getMyPurchaseCallback) {

        mPurchaseRemoteDataSource.getMyPurchases(apiVersion,
                new GetMyPurchaseCallback() {
                    @Override
                    public void onGetMyPurchaseSuccess(@NonNull GetMyPurchaseResponse
                                                               getMyPurchaseResponse) {

                        getMyPurchaseCallback.onGetMyPurchaseSuccess(getMyPurchaseResponse);
                    }

                    @Override
                    public void onGetMyPurchaseFail(@NonNull String errorModel) {
                        getMyPurchaseCallback.onGetMyPurchaseFail(errorModel);
                    }
                });
    }
}
