package com.soulplatform.consumableitems.data.repository.purchase.source.remote;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.soulplatform.consumableitems.data.network.ApiManager;
import com.soulplatform.consumableitems.data.repository.purchase.PurchaseDataSource;
import com.soulplatform.consumableitems.domain.model.purchase.bundle.GetAllBundleResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeItemResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.GetMyPurchaseResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderResponse;
import com.soulplatform.consumableitems.utils.Preconditions;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class PurchaseRemoteDataSource implements PurchaseDataSource {

    private final Context mContext;
    private static PurchaseRemoteDataSource sPurchaseRemoteDataSource = null;

    private PurchaseRemoteDataSource(@NonNull Context context) {
        mContext = Preconditions.checkNotNull(context);
    }

    public static PurchaseRemoteDataSource getInstance(@NonNull Context context) {
        if (sPurchaseRemoteDataSource == null) {
            synchronized (PurchaseRemoteDataSource.class) {
                if (sPurchaseRemoteDataSource == null) {
                    sPurchaseRemoteDataSource = new PurchaseRemoteDataSource(context);

                }
            }
        }
        return sPurchaseRemoteDataSource;
    }

    @Override
    public void getAllBundles(@NonNull String apiVersion, @NonNull final GetAllBundleCallback
            getAllBundleCallback) {

        ApiManager.getAllBundles(mContext, apiVersion, new Callback<GetAllBundleResponse>() {
            @Override
            public void onResponse(Call<GetAllBundleResponse> call,
                                   Response<GetAllBundleResponse> response) {

                if (response != null && response.isSuccessful() &&
                        response.body() != null) {
                    getAllBundleCallback.onGetAllBundleSuccess(response.body());
                } else {
                    try {
                        getAllBundleCallback.onGetAllBundleFail(response.errorBody().string());
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetAllBundleResponse> call, Throwable t) {
                t.printStackTrace();
                getAllBundleCallback.onGetAllBundleFail("");
            }
        });
    }

    @Override
    public void consumeItem(@NonNull String itemId, int quantity, @NonNull String apiVersion,
                            @NonNull final ConsumeItemCallback consumeItemCallback) {

        ApiManager.consumeItem(mContext, itemId, quantity, apiVersion,
                new Callback<ConsumeItemResponse>() {
                    @Override
                    public void onResponse(Call<ConsumeItemResponse> call,
                                           Response<ConsumeItemResponse> response) {

                        if (response != null && response.isSuccessful() &&
                                response.body() != null) {

                            consumeItemCallback.onConsumeItemSuccess(response.body());
                        } else {

                            try {
                                consumeItemCallback.onConsumeItemFail(response.errorBody().string());
                            } catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ConsumeItemResponse> call, Throwable t) {
                        t.printStackTrace();
                        consumeItemCallback.onConsumeItemFail("");
                    }
                });
    }

    @Override
    public void validateOrder(@NonNull String token, @NonNull String subscriptionId,
                              @NonNull String signature, @NonNull String apiVersion,
                              @NonNull final ValidateOrderCallback validateOrderCallback) {

        ApiManager.validateOrder(mContext, token, subscriptionId, signature, apiVersion,
                new Callback<ValidateOrderResponse>() {
                    @Override
                    public void onResponse(Call<ValidateOrderResponse> call,
                                           Response<ValidateOrderResponse> response) {

                        if (response != null && response.isSuccessful() &&
                                response.body() != null) {

                            validateOrderCallback.onValidateOrderSuccess(response.body());
                        } else {
                            try {
                                validateOrderCallback.onValidateOrderFail(response.errorBody().string());
                            } catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ValidateOrderResponse> call, Throwable t) {
                        t.printStackTrace();
                        validateOrderCallback.onValidateOrderFail("");
                    }
                });
    }

    @Override
    public void getMyPurchases(@NonNull String apiVersion,
                               @NonNull final GetMyPurchaseCallback getMyPurchaseCallback) {

        ApiManager.getMyPurchases(mContext, apiVersion,
                new Callback<GetMyPurchaseResponse>() {
                    @Override
                    public void onResponse(Call<GetMyPurchaseResponse> call,
                                           Response<GetMyPurchaseResponse> response) {

                        if (response != null && response.isSuccessful() &&
                                response.body() != null) {

                            getMyPurchaseCallback.onGetMyPurchaseSuccess(response.body());
                        } else {
                            try {
                                getMyPurchaseCallback.onGetMyPurchaseFail(response.errorBody().string());
                            } catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetMyPurchaseResponse> call, Throwable t) {
                        t.printStackTrace();
                        getMyPurchaseCallback.onGetMyPurchaseFail("");
                    }
                });
    }
}
