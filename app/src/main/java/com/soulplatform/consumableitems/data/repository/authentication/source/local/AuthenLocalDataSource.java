package com.soulplatform.consumableitems.data.repository.authentication.source.local;

import android.content.Context;
import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.data.repository.authentication.AuthenDataSource;
import com.soulplatform.consumableitems.utils.Preconditions;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class AuthenLocalDataSource implements AuthenDataSource {

    private static AuthenLocalDataSource sAuthenLocalDataSource = null;

    private final Context mContext;

    private AuthenLocalDataSource(@NonNull Context context) {
        mContext = Preconditions.checkNotNull(context);
    }

    public static AuthenLocalDataSource getInstance(@NonNull Context context) {
        if (sAuthenLocalDataSource == null) {
            synchronized (AuthenLocalDataSource.class) {
                if (sAuthenLocalDataSource == null) {
                    sAuthenLocalDataSource = new AuthenLocalDataSource(context);
                }
            }
        }
        return sAuthenLocalDataSource;
    }

    @Override
    public void login(@NonNull String userName, @NonNull String password, @NonNull String apiKey,
                      @NonNull String apiVersion, @NonNull LoginCallback loginCallback) {

    }
}
