package com.soulplatform.consumableitems.data.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.soulplatform.consumableitems.BuildConfig;
import com.soulplatform.consumableitems.domain.model.authentication.login.LoginRequestModel;
import com.soulplatform.consumableitems.domain.model.authentication.login.LoginResponseModel;
import com.soulplatform.consumableitems.domain.model.purchase.bundle.GetAllBundleResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeItemResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeRequestModel;
import com.soulplatform.consumableitems.domain.model.purchase.item.GetMyPurchaseResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderRequest;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderResponse;
import com.soulplatform.consumableitems.utils.Const;
import com.soulplatform.consumableitems.utils.MyPreferences;
import com.soulplatform.consumableitems.utils.StringUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by "Tuan Nguyen" on 11/10/2016.
 */

public class ApiManager {


    private static final String BASE_URL = "https://stage-api.soulplatform.com/";
    private static ApiEndpointInterface mApiEndpoint;
    private static volatile Retrofit mRetrofit;

    private static final String HEADER_KEY_USER_AGENT = "User-Agent";
    private static final String HEADER_VALUE_USER_AGENT = "consumabletest/2.7.0 (Android 7.0; " +
            "Redmi Note 4;" +
            " en_US; NRD90M) SoulSDK/2.7.0 (Android)";

    /**
     * Use this method to obtaining Retrofit instance
     *
     * @param context The context where Retrofit instance is used
     * @return a singleton instance of Retrofit
     */
    private static Retrofit getRetrofit(final Context context) {
        if (mRetrofit == null) {
            synchronized (ApiManager.class) {
                if (mRetrofit == null) {
                    //Set log
                    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                    final boolean isLog = BuildConfig.DEBUG;
                    logging.setLevel(isLog ? HttpLoggingInterceptor.Level.BODY :
                            HttpLoggingInterceptor.Level.NONE);
                    //Create cache
                    File file = new File(context.getCacheDir(), "response");

                    //Add log and set time out
                    final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                            .readTimeout(60, TimeUnit.SECONDS)
                            //.connectTimeout(60, TimeUnit.SECONDS)
//                            .cache(new Cache(file, 10 * 1024 * 1024)) //10 MB
//                            .addNetworkInterceptor(new AddHeaderInterceptor())
                            .addInterceptor(logging)
                            .retryOnConnectionFailure(true)
                            .build();


                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();

                    mRetrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
//                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(okHttpClient).build();

                }
            }
        }

        return mRetrofit;
    }

    /**
     * Use this method to obtain the Api endpoint instance where Retrofit request definitions are
     * declared
     *
     * @param context The context where APIs are consumed
     * @return The Api endpoint instance to access network requests
     */
    private static ApiEndpointInterface getApiEndpoint(Context context) {
        if (mApiEndpoint == null) {
            synchronized (ApiManager.class) {
                if (mApiEndpoint == null) {
                    mApiEndpoint = getRetrofit(context).create(ApiEndpointInterface.class);
                }
            }
        }
        return mApiEndpoint;
    }

    public static void login(@NonNull Context context,
                             @NonNull String userName,
                             @NonNull String password,
                             @NonNull String apiKey,
                             @NonNull String apiVersion,
                             @NonNull Callback<LoginResponseModel> loginResponseModelCallback) {

        LoginRequestModel loginRequestModel = new LoginRequestModel(userName, password, apiKey);
        Call<LoginResponseModel> call = getApiEndpoint(context).login(loginRequestModel, apiVersion,
                HEADER_VALUE_USER_AGENT);
        call.enqueue(loginResponseModelCallback);
    }

    public static void getAllBundles(@NonNull Context context,
                                     @NonNull String apiVersion,
                                     @NonNull Callback<GetAllBundleResponse>
                                             getAllBundleResponseCallback) {

        long currentTime = System.currentTimeMillis() / 1000;
        Call<GetAllBundleResponse> call = getApiEndpoint(context).getAllBundles(apiVersion,
                "hmac " + MyPreferences.getUserId() + ":" +
                        String.valueOf(currentTime) + ":" +
                        StringUtils.hmacSha256Hex(MyPreferences.getSessionToken(),
                                "GET",
                                "/purchases/all?v=" + Const.API_VERSION,
                                null,
                                String.valueOf(currentTime)),
                HEADER_VALUE_USER_AGENT);
        call.enqueue(getAllBundleResponseCallback);
    }

    public static void consumeItem(@NonNull Context context,
                                   @NonNull String itemId,
                                   @NonNull int quantity,
                                   @NonNull String apiVersion,
                                   @NonNull Callback<ConsumeItemResponse>
                                           consumeItemResponseCallback) {

        long currentTime = System.currentTimeMillis() / 1000;

        ConsumeRequestModel consumeRequestModel = new ConsumeRequestModel(itemId, quantity);
        Gson gson = new Gson();
        String consumeRequestString = gson.toJson(consumeRequestModel);
        RequestBody consumeRequestBody = RequestBody.create(MediaType.parse(Const.MEDIA_TYPE_JSON),
                consumeRequestString);

        Call<ConsumeItemResponse> call = getApiEndpoint(context).consumeItem(consumeRequestModel,
                apiVersion,
                "hmac " + MyPreferences.getUserId() + ":" +
                        String.valueOf(currentTime) + ":" +
                        StringUtils.hmacSha256Hex(MyPreferences.getSessionToken(),
                                "POST",
                                "/purchases/consume?v=" + Const.API_VERSION,
                                consumeRequestString,
                                String.valueOf(currentTime)),
                HEADER_VALUE_USER_AGENT);
        call.enqueue(consumeItemResponseCallback);
    }

    public static void validateOrder(@NonNull Context context,
                                     @NonNull String token,
                                     @NonNull String subscriptionId,
                                     @NonNull String signature,
                                     @NonNull String apiVersion,
                                     @NonNull Callback<ValidateOrderResponse>
                                             validateOrderResponseCallback) {

        long currentTime = System.currentTimeMillis() / 1000;

        ValidateOrderRequest validateOrderRequest = new ValidateOrderRequest(token,
                subscriptionId, signature);

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String validateRequestString = gson.toJson(validateOrderRequest);
        RequestBody validateRequestBody = RequestBody.create(MediaType.parse(Const.MEDIA_TYPE_JSON),
                validateRequestString);

        Log.i("tuancoltech", "validateRequestBody: " + validateRequestBody + ", string: " + validateRequestBody.toString() + ", jsonString: " + validateRequestString);

        Log.i("tuancoltech2", "userId: " + MyPreferences.getUserId());
        Log.i("tuancoltech2", "unixTime: " + currentTime);
        Log.i("tuancoltech2", "sessionToken: " + MyPreferences.getSessionToken());
        Log.i("tuancoltech2", "completePath: " + "/purchases/order/googleplay?v=" + Const.API_VERSION);
        Log.i("tuancoltech2", "body: " + validateRequestString);

        String concatString = StringUtils.getConcatString("POST",
                "/purchases/order/googleplay?v=" + Const.API_VERSION,
                validateRequestString,
                String.valueOf(currentTime));

        String digest = StringUtils.hmacSha256(MyPreferences.getSessionToken(), concatString);

        Call<ValidateOrderResponse> call = getApiEndpoint(context).validateOrder
                (validateOrderRequest,
                        apiVersion,
                        "hmac " + MyPreferences.getUserId() + ":" +
                                String.valueOf(currentTime) + ":" +
                                /*StringUtils.hmacSha256Hex(MyPreferences.getSessionToken(),
                                        "POST",
                                        "/purchases/order/googleplay?v=" + Const.API_VERSION,
                                        validateRequestString,
                                        String.valueOf(currentTime))*/digest,
                        HEADER_VALUE_USER_AGENT);
        call.enqueue(validateOrderResponseCallback);
    }

    public static void getMyPurchases(@NonNull Context context,
                                      @NonNull String apiVersion,
                                      @NonNull Callback<GetMyPurchaseResponse>
                                              getMyPurchaseResponseCallback) {

        long currentTime = System.currentTimeMillis() / 1000;
        Call<GetMyPurchaseResponse> call = getApiEndpoint(context).getMyPurchases(apiVersion,
                "hmac " + MyPreferences.getUserId() + ":" +
                        String.valueOf(currentTime) + ":" +
                        StringUtils.hmacSha256Hex(MyPreferences.getSessionToken(),
                                "GET",
                                "/purchases/my?v=" + Const.API_VERSION,
                                null,
                                String.valueOf(currentTime)),
                HEADER_VALUE_USER_AGENT);
        call.enqueue(getMyPurchaseResponseCallback);
    }

}

