package com.soulplatform.consumableitems.data.network;

import com.soulplatform.consumableitems.domain.model.authentication.login.LoginRequestModel;
import com.soulplatform.consumableitems.domain.model.authentication.login.LoginResponseModel;
import com.soulplatform.consumableitems.domain.model.purchase.bundle.GetAllBundleResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeItemResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeRequestModel;
import com.soulplatform.consumableitems.domain.model.purchase.item.GetMyPurchaseResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderRequest;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by "Tuan Nguyen" on 11/10/2016.
 */

interface ApiEndpointInterface {

    @POST("/auth/password/login")
    Call<LoginResponseModel> login(@Field("login") String userName,
                                   @Field("password") String password,
                                   @Field("apiKey") String apiKey,
                                   @Query("v") String apiVersion);

    @POST("auth/password/login")
    Call<LoginResponseModel> login(@Body LoginRequestModel loginRequestModel,
                                   @Query("v") String apiVersion,
                                   @Header("user-agent") String userAgent);

    @GET("/purchases/all")
    Call<GetAllBundleResponse> getAllBundles(@Query("v") String apiVersion,
                                             @Header("Authorization") String hmacString,
                                             @Header("user-agent") String userAgent);

    @POST("/purchases/consume")
    Call<ConsumeItemResponse> consumeItem(@Body ConsumeRequestModel consumeRequestModel,
                                          @Query("v") String apiVersion,
                                          @Header("Authorization") String hmacString,
                                          @Header("user-agent") String userAgent);

    @POST("/purchases/order/googleplay")
    Call<ValidateOrderResponse> validateOrder(@Body ValidateOrderRequest validateOrderRequest,
                                              @Query("v") String apiVersion,
                                              @Header("Authorization") String hmacString,
                                              @Header("user-agent") String userAgent);

    @GET("/purchases/my")
    Call<GetMyPurchaseResponse> getMyPurchases(@Query("v") String apiVersion,
                                               @Header("Authorization") String hmacString,
                                               @Header("user-agent") String userAgent);

}