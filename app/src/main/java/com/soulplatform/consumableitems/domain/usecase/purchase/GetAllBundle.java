package com.soulplatform.consumableitems.domain.usecase.purchase;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.data.repository.purchase.PurchaseDataSource;
import com.soulplatform.consumableitems.data.repository.purchase.source.PurchaseRepository;
import com.soulplatform.consumableitems.domain.model.purchase.bundle.GetAllBundleResponse;
import com.soulplatform.consumableitems.mvpcore.UseCase;
import com.soulplatform.consumableitems.utils.Preconditions;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class GetAllBundle extends UseCase<GetAllBundle.RequestValue, GetAllBundle.ResponseValue>{

    private final PurchaseRepository mPurchaseRepository;

    public GetAllBundle(@NonNull PurchaseRepository purchaseRepository) {
        mPurchaseRepository = Preconditions.checkNotNull(purchaseRepository);
    }

    @Override
    protected void executeUseCase(RequestValue requestValues) {
        Preconditions.checkNotNull(requestValues);
        Preconditions.checkNotNull(mPurchaseRepository);

        mPurchaseRepository.getAllBundles(requestValues.getApiVersion(),
                new PurchaseDataSource.GetAllBundleCallback() {
                    @Override
                    public void onGetAllBundleSuccess(@NonNull GetAllBundleResponse getAllBundleResponse) {
                        getUseCaseCallback().onSuccess(new ResponseValue(getAllBundleResponse));
                    }

                    @Override
                    public void onGetAllBundleFail(@NonNull String errorModel) {
                        getUseCaseCallback().onError(errorModel);
                    }
                });
    }

    public static final class RequestValue implements UseCase.RequestValues {

        private final String mApiVersion;

        public String getApiVersion() {
            return mApiVersion;
        }

        public RequestValue(String apiVersion) {
            mApiVersion = apiVersion;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final GetAllBundleResponse mGetAllBundleResponse;

        public GetAllBundleResponse getGetAllBundleResponse() {
            return mGetAllBundleResponse;
        }

        public ResponseValue(GetAllBundleResponse getAllBundleResponse) {

            mGetAllBundleResponse = getAllBundleResponse;
        }
    }

}
