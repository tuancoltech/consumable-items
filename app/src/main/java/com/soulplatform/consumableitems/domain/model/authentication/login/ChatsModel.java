package com.soulplatform.consumableitems.domain.model.authentication.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class ChatsModel {

    @SerializedName("notDeletedByMe")
    private int mNotDeletedByMe;

    @SerializedName("notDeletedByAnyone")
    private int mNotDeletedByAnyone;

    @SerializedName("notExpiredAndNotDeletedByAnyone")
    private int mNotExpiredAndNotDeletedByAnyone;

}
