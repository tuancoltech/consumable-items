package com.soulplatform.consumableitems.domain.usecase.purchase;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.data.repository.purchase.PurchaseDataSource;
import com.soulplatform.consumableitems.data.repository.purchase.source.PurchaseRepository;
import com.soulplatform.consumableitems.domain.model.purchase.item.GetMyPurchaseResponse;
import com.soulplatform.consumableitems.mvpcore.UseCase;
import com.soulplatform.consumableitems.utils.Const;
import com.soulplatform.consumableitems.utils.Preconditions;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/18/2017.
 */

public class GetMyPurchases extends UseCase<GetMyPurchases.RequestValue,
        GetMyPurchases.ResponseValue> {

    private final PurchaseRepository mPurchaseRepository;

    public GetMyPurchases(PurchaseRepository purchaseRepository) {
        mPurchaseRepository = Preconditions.checkNotNull(purchaseRepository);
    }

    @Override
    protected void executeUseCase(RequestValue requestValues) {
        Preconditions.checkNotNull(requestValues);
        mPurchaseRepository.getMyPurchases(requestValues.getApiVersion(),
                new PurchaseDataSource.GetMyPurchaseCallback() {
                    @Override
                    public void onGetMyPurchaseSuccess(@NonNull GetMyPurchaseResponse
                                                               getMyPurchaseResponse) {
                        getUseCaseCallback().onSuccess(new ResponseValue(getMyPurchaseResponse));
                    }

                    @Override
                    public void onGetMyPurchaseFail(@NonNull String errorModel) {
                        getUseCaseCallback().onError(errorModel);
                    }
                });
    }

    public static final class RequestValue implements UseCase.RequestValues {

        private final String mApiVersion;

        public String getApiVersion() {
            return mApiVersion;
        }

        public RequestValue(String apiVersion) {

            mApiVersion = apiVersion;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final GetMyPurchaseResponse mGetMyPurchaseResponse;

        public GetMyPurchaseResponse getGetMyPurchaseResponse() {
            return mGetMyPurchaseResponse;
        }

        public ResponseValue(GetMyPurchaseResponse getMyPurchaseResponse) {

            mGetMyPurchaseResponse = getMyPurchaseResponse;
        }
    }

}
