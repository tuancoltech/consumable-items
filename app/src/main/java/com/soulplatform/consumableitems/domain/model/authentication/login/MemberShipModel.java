package com.soulplatform.consumableitems.domain.model.authentication.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class MemberShipModel {

    @SerializedName("subscriptionId")
    private String mSubscriptionId;

    @SerializedName("expiresTime")
    private float mExpireTime;

}
