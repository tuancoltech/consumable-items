package com.soulplatform.consumableitems.domain.usecase.purchase;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.data.repository.purchase.PurchaseDataSource;
import com.soulplatform.consumableitems.data.repository.purchase.source.PurchaseRepository;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderResponse;
import com.soulplatform.consumableitems.mvpcore.UseCase;
import com.soulplatform.consumableitems.utils.Const;
import com.soulplatform.consumableitems.utils.Preconditions;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/18/2017.
 */

public class ValidateOrder extends UseCase<ValidateOrder.RequestValue, ValidateOrder
        .ResponseValue> {

    private final PurchaseRepository mPurchaseRepository;

    public ValidateOrder(PurchaseRepository purchaseRepository) {
        mPurchaseRepository = Preconditions.checkNotNull(purchaseRepository);
    }

    @Override
    protected void executeUseCase(RequestValue requestValues) {
        Preconditions.checkNotNull(requestValues);
        mPurchaseRepository.validateOrder(requestValues.getToken(),
                requestValues.getSubscriptionId(),
                requestValues.getSignature(),
                requestValues.getApiVersion(),
                new PurchaseDataSource.ValidateOrderCallback() {
                    @Override
                    public void onValidateOrderSuccess(@NonNull ValidateOrderResponse
                                                               validateOrderResponse) {
                        getUseCaseCallback().onSuccess(new ResponseValue(validateOrderResponse));
                    }

                    @Override
                    public void onValidateOrderFail(@NonNull String errorModel) {
                        getUseCaseCallback().onError(errorModel);
                    }
                });
    }

    public static final class RequestValue implements UseCase.RequestValues {

        private final String mToken;
        private final String mSubscriptionId;
        private final String mSignature;
        private final String mApiVersion;

        public String getToken() {
            return mToken;
        }

        public String getSubscriptionId() {
            return mSubscriptionId;
        }

        public String getSignature() {
            return mSignature;
        }

        public String getApiVersion() {
            return mApiVersion;
        }

        public RequestValue(String token, String subscriptionId, String signature,
                            String apiVersion) {

            mToken = token;
            mSubscriptionId = subscriptionId;
            mSignature = signature;
            mApiVersion = Preconditions.checkNotNull(apiVersion);
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final ValidateOrderResponse mValidateOrderResponse;

        public ValidateOrderResponse getValidateOrderResponse() {
            return mValidateOrderResponse;
        }

        public ResponseValue(ValidateOrderResponse validateOrderResponse) {

            mValidateOrderResponse = validateOrderResponse;
        }
    }

}
