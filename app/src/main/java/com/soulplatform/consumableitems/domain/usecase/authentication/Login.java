package com.soulplatform.consumableitems.domain.usecase.authentication;

import android.support.annotation.NonNull;
import android.util.Log;

import com.soulplatform.consumableitems.data.repository.authentication.AuthenDataSource;
import com.soulplatform.consumableitems.data.repository.authentication.source.AuthenRepository;
import com.soulplatform.consumableitems.domain.model.authentication.login.ErrorModel;
import com.soulplatform.consumableitems.domain.model.authentication.login.LoginResponseModel;
import com.soulplatform.consumableitems.mvpcore.UseCase;
import com.soulplatform.consumableitems.utils.Preconditions;

import java.io.IOException;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class Login extends UseCase<Login.RequestValue, Login.ResponseValue> {

    private final AuthenRepository mAuthenRepository;

    public Login(@NonNull AuthenRepository authenRepository) {
        mAuthenRepository = Preconditions.checkNotNull(authenRepository);
    }

    @Override
    protected void executeUseCase(RequestValue requestValues) {
        Preconditions.checkNotNull(requestValues);
        Preconditions.checkNotNull(mAuthenRepository);

        mAuthenRepository.login(requestValues.getUserName(),
                requestValues.getPassword(),
                requestValues.getApiKey(),
                requestValues.getApiVersion(),
                new AuthenDataSource.LoginCallback() {
                    @Override
                    public void onLoginSuccess(@NonNull LoginResponseModel loginResponseModel) {
                        getUseCaseCallback().onSuccess(new ResponseValue(loginResponseModel));
                    }

                    @Override
                    public void onLoginFail(@NonNull String errorModel) {
                        Log.i("tuancoltech", " response error msg 3: " + errorModel);
                        getUseCaseCallback().onError(errorModel);
                    }
                });
    }

    public static final class RequestValue implements UseCase.RequestValues {

        private final String mUserName;

        private final String mPassword;

        private final String mApiKey;

        private final String mApiVersion;

        public String getUserName() {
            return mUserName;
        }

        public String getPassword() {
            return mPassword;
        }

        public String getApiKey() {
            return mApiKey;
        }

        public String getApiVersion() {
            return mApiVersion;
        }

        public RequestValue(String userName, String password, String apiKey, String apiVersion) {

            mUserName = userName;
            mPassword = password;
            mApiKey = apiKey;
            mApiVersion = apiVersion;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final LoginResponseModel mLoginResponseModel;

        public LoginResponseModel getLoginResponseModel() {
            return mLoginResponseModel;
        }

        public ResponseValue(LoginResponseModel loginResponseModel) {

            mLoginResponseModel = loginResponseModel;
        }
    }

}
