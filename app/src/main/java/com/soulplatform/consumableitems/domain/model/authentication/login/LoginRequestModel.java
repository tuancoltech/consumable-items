package com.soulplatform.consumableitems.domain.model.authentication.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class LoginRequestModel {

    @SerializedName("login")
    private String mUserName;

    @SerializedName("passwd")
    private String mPassword;

    @SerializedName("apiKey")
    private String mApiKey;

    public LoginRequestModel(String userName, String password, String apiKey) {
        mUserName = userName;
        mPassword = password;
        mApiKey = apiKey;
    }

    public String getUserName() {
        return mUserName;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getApiKey() {
        return mApiKey;
    }

}
