package com.soulplatform.consumableitems.domain.model.purchase.bundle;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class BundleModel {

    @SerializedName("bundleName")
    private String mBundleName;

    @SerializedName("hasTrial")
    private boolean mHasTrial;

    @SerializedName("order")
    private int mOrder;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("purchaseCount")
    private int mPurchaseCount;

    @SerializedName("products")
    private List<BundleProductModel> mProducts;

    public String getBundleName() {
        return mBundleName;
    }

    public boolean isHasTrial() {
        return mHasTrial;
    }

    public int getOrder() {
        return mOrder;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getPurchaseCount() {
        return mPurchaseCount;
    }

    public List<BundleProductModel> getProducts() {
        return mProducts;
    }
}
