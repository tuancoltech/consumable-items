package com.soulplatform.consumableitems.domain.model.authentication.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class AuthorizationModel {

    @SerializedName("sessionToken")
    private String mSessionToken;

    @SerializedName("expiresTime")
    private float mExpireTime;

    public String getSessionToken() {
        return mSessionToken;
    }

    public float getExpireTime() {
        return mExpireTime;
    }
}
