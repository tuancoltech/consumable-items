package com.soulplatform.consumableitems.domain.model.purchase.item;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Tuan Nguyen on 12/18/2017.
 */

public class UserItemModel {

    @SerializedName("id")
    private String mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("type")
    private String mType;

    @SerializedName("validBefore")
    private long mValidBefore;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("quantity")
    private int mQuantity;

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getType() {
        return mType;
    }

    public long getValidBefore() {
        return mValidBefore;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getQuantity() {
        return mQuantity;
    }
}
