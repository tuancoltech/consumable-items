package com.soulplatform.consumableitems.domain.model.authentication.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class ErrorModel {

    @SerializedName("alias")
    private String mAlias;

    @SerializedName("code")
    private int mCode;

    @SerializedName("developerMessage")
    private String mDeveloperMessage;

    @SerializedName("userMessage")
    private String mUserMessage;

    public ErrorModel() {
        mCode = -1;
    }

    public int getCode() {
        return mCode;
    }

}
