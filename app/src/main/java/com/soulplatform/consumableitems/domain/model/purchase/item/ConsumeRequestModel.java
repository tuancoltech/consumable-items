package com.soulplatform.consumableitems.domain.model.purchase.item;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

/**
 * Created by Tuan Nguyen on 12/18/2017.
 */

public class ConsumeRequestModel {

    @SerializedName("itemId")
    private String mItemId;

    @SerializedName("quantity")
    private int mQuantity;

    public String getItemId() {
        return mItemId;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public String getConsumptionId() {
        return mConsumptionId;
    }

    @SerializedName("consumptionId")
    private String mConsumptionId;

    public ConsumeRequestModel(String itemId, int quantity) {
        mItemId = itemId;
        mQuantity = quantity;
        mConsumptionId = UUID.randomUUID().toString();
    }
}
