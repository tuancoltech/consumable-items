package com.soulplatform.consumableitems.domain.model.purchase.item;

import com.google.gson.annotations.SerializedName;
import com.soulplatform.consumableitems.domain.model.authentication.login.AdditionalInfoModel;

import java.util.List;

/**
 * Created by Tuan Nguyen on 12/18/2017.
 */

public class ConsumeItemResponse {

    @SerializedName("bundles")
    private List<String> mBundleNames;

    @SerializedName("items")
    private List<UserItemModel> mUserItems;

    @SerializedName("additionalInfo")
    private AdditionalInfoModel mAdditionalInfoModel;

    public List<String> getBundleNames() {
        return mBundleNames;
    }

    public List<UserItemModel> getUserItems() {
        return mUserItems;
    }

    public AdditionalInfoModel getAdditionalInfoModel() {
        return mAdditionalInfoModel;
    }
}
