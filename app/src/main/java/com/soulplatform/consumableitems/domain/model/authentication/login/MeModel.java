package com.soulplatform.consumableitems.domain.model.authentication.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class MeModel {

    @SerializedName("id")
    private String mId;

    @SerializedName("notificationTokens")
    private NotificationTokenModel mNotificationTokenModel;

    @SerializedName("subscriptionServices")
    private SubscriptionServiceModel mSubscriptionServiceModel;

    public String getId() {
        return mId;
    }

}
