package com.soulplatform.consumableitems.domain.model.purchase.bundle;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Tuan Nguyen on 12/19/2017.
 */

public class BundleProductModel {

    @SerializedName("description")
    private String mDescription;

    @SerializedName("lifeTimeSeconds")
    private long mLifeTimeSeconds;

    @SerializedName("name")
    private String mName;

    @SerializedName("trial")
    private boolean mTrial;

    @SerializedName("type")
    private String mType;

    @SerializedName("quantity")
    private int mQuantity;

}
