package com.soulplatform.consumableitems.domain.model.authentication.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class LoginResponseModel {

    @SerializedName("authorization")
    private AuthorizationModel mAuthorizationModel;

    @SerializedName("me")
    private MeModel mMeModel;

    @SerializedName("objectCount")
    private ObjectCountModel mObjectCountModel;

    @SerializedName("providerId")
    private String mProviderId;

    @SerializedName("additionalInfo")
    private AdditionalInfoModel mAdditionalInfoModel;

    @SerializedName("error")
    private ErrorModel mErrorModel;

    public AuthorizationModel getAuthorizationModel() {
        return mAuthorizationModel;
    }

    public MeModel getMeModel() {
        return mMeModel;
    }

    public ErrorModel getErrorModel() {
        return mErrorModel;
    }

    public AdditionalInfoModel getAdditionalInfoModel() {
        return mAdditionalInfoModel;
    }
}
