package com.soulplatform.consumableitems.domain.model.purchase.item;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

/**
 * Created by Tuan Nguyen on 12/18/2017.
 */

public class ValidateOrderRequest {

    @SerializedName("token")
    private String mToken;

    @SerializedName("productId")
    private String mSubscriptionId;

    @SerializedName("signature")
    private String mSignature;

    public String getToken() {
        return mToken;
    }

    public String getSubscriptionId() {
        return mSubscriptionId;
    }

    public String getSignature() {
        return mSignature;
    }

    public ValidateOrderRequest(String token, String subscriptionId, String signature) {

        mToken = token;
        mSubscriptionId = subscriptionId;
        mSignature = signature;
    }
}
