package com.soulplatform.consumableitems.domain.model.purchase.bundle;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class GetAllBundleResponse {

    @SerializedName("bundles")
    private List<BundleModel> mBundleData;

    public List<BundleModel> getBundleData() {
        return mBundleData;
    }
}
