package com.soulplatform.consumableitems.domain.usecase.purchase;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.data.repository.purchase.PurchaseDataSource;
import com.soulplatform.consumableitems.data.repository.purchase.source.PurchaseRepository;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeItemResponse;
import com.soulplatform.consumableitems.mvpcore.UseCase;
import com.soulplatform.consumableitems.utils.Const;
import com.soulplatform.consumableitems.utils.Preconditions;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/18/2017.
 */

public class ConsumeItem extends UseCase<ConsumeItem.RequestValue, ConsumeItem.ResponseValue> {

    private final PurchaseRepository mPurchaseRepository;

    public ConsumeItem(@NonNull PurchaseRepository purchaseRepository) {
        mPurchaseRepository = Preconditions.checkNotNull(purchaseRepository);
    }

    @Override
    protected void executeUseCase(RequestValue requestValues) {
        Preconditions.checkNotNull(requestValues);
        Preconditions.checkNotNull(mPurchaseRepository);

        mPurchaseRepository.consumeItem(requestValues.getItemId(),
                requestValues.getQuantity(),
                requestValues.getApiVersion(),
                new PurchaseDataSource.ConsumeItemCallback() {
                    @Override
                    public void onConsumeItemSuccess(@NonNull ConsumeItemResponse
                                                             consumeItemResponse) {
                        getUseCaseCallback().onSuccess(new ResponseValue(consumeItemResponse));
                    }

                    @Override
                    public void onConsumeItemFail(@NonNull String errorModel) {
                        getUseCaseCallback().onError(errorModel);
                    }
                });
    }

    public static final class RequestValue implements UseCase.RequestValues {

        private final String mItemId;
        private final int mQuantity;
        private final String mApiVersion;

        public String getItemId() {
            return mItemId;
        }

        public int getQuantity() {
            return mQuantity;
        }

        public String getApiVersion() {
            return mApiVersion;
        }

        public RequestValue(String itemId, int quantity, String apiVersion) {

            mItemId = itemId;
            mQuantity = quantity;
            mApiVersion = apiVersion;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final ConsumeItemResponse mConsumeItemResponse;

        public ConsumeItemResponse getConsumeItemResponse() {
            return mConsumeItemResponse;
        }

        public ResponseValue(ConsumeItemResponse consumeItemResponse) {

            mConsumeItemResponse = consumeItemResponse;
        }
    }

}
