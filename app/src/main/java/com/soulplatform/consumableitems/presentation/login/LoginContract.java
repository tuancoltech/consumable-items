package com.soulplatform.consumableitems.presentation.login;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.domain.model.authentication.login.LoginResponseModel;
import com.soulplatform.consumableitems.presentation.common.BasePresenter;
import com.soulplatform.consumableitems.presentation.common.BaseView;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public interface LoginContract {

    interface View extends BaseView<Presenter> {

        void setLoading(boolean isActive);

        void showLoginSuccess(@NonNull LoginResponseModel loginResponseModel);

        void showLoginError(String errorCode);

    }

    interface Presenter extends BasePresenter {

        void login(@NonNull String userName,
                   @NonNull String password,
                   @NonNull String apiKey,
                   @NonNull String apiVersion);

    }

}
