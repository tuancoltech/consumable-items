package com.soulplatform.consumableitems.presentation.purchase;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.domain.model.purchase.bundle.GetAllBundleResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeItemResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.GetMyPurchaseResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderResponse;
import com.soulplatform.consumableitems.presentation.common.BasePresenter;
import com.soulplatform.consumableitems.presentation.common.BaseView;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public interface PurchaseTestContract {

    interface Presenter extends BasePresenter {

        void getAllBundle(@NonNull String apiVersion);

        void getMyItems(@NonNull String apiVersion);

        void validateOrder(@NonNull String purchaseToken,
                           @NonNull String skuId,
                           @NonNull String signature,
                           @NonNull String apiVersion);

        void consumeItem(@NonNull String skuId,
                         @NonNull int quantity,
                         @NonNull String apiVersion);

    }

    interface View extends BaseView<Presenter> {

        void setLoading(boolean isActive);

        void showGetAllBundleSuccess(@NonNull GetAllBundleResponse getAllBundleResponse);

        void showGetAllBundleFail(String errorCode);

        void showGetMyItemSuccess(@NonNull GetMyPurchaseResponse getMyPurchaseResponse);

        void showGetMyItemFail(String errorCode);

        void showValidateOrderSuccess(@NonNull ValidateOrderResponse validateOrderResponse);

        void showValidateOrderFail(String errorCode);

        void showConsumeItemSuccess(@NonNull ConsumeItemResponse consumeItemResponse);

        void showConsumeItemFail(String errorCode);

    }

}
