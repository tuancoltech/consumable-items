package com.soulplatform.consumableitems.presentation.purchase;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.soulplatform.consumableitems.R;
import com.soulplatform.consumableitems.domain.model.purchase.bundle.BundleModel;
import com.soulplatform.consumableitems.domain.model.purchase.bundle.GetAllBundleResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.ConsumeItemResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.GetMyPurchaseResponse;
import com.soulplatform.consumableitems.domain.model.purchase.item.UserItemModel;
import com.soulplatform.consumableitems.domain.model.purchase.item.ValidateOrderResponse;
import com.soulplatform.consumableitems.mvpcore.Injection;
import com.soulplatform.consumableitems.presentation.common.BaseActivity;
import com.soulplatform.consumableitems.utils.Const;
import com.soulplatform.consumableitems.utils.Preconditions;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/14/2017.
 */

public class PurchaseTestActivity extends BaseActivity implements PurchasesUpdatedListener,
        PurchaseTestContract.View, View.OnClickListener {

    private BillingClient mBillingClient;
    private PurchaseTestContract.Presenter mPurchasePresenter;

    private final String SKU_ID_1 = "android.com.soulplatform.boost1";
    private final String SKU_ID_2 = "android.com.soulplatform.boost5";

    private RecyclerView mRecyclerBundles;
    private BundleAdapter mAdapterBundle;
    private List<BundleModel> mBundleData;

    private RecyclerView mRecyclerInventory;
    private InventoryAdapter mAdapterInventory;

    private Button mBtnBuySku1;
    private Button mBtnBuySku2;

    private TextView mTvEmptyBundles;
    private TextView mTvEmptyInventory;

    private List<Purchase> mPurchaseList;
    private List<String> mDataMyInventory;

    private BundleAdapter.ItemProcessCallback mItemProcessCallback;
    private String mLastConsumeSku = null;

    private TextView mTextItemQuantity;

    private String mInventoryItemId = "";

    private ConsumeResponseListener mConsumeResponseListener = new ConsumeResponseListener() {
        @Override
        public void onConsumeResponse(int responseCode, String purchaseToken) {
            if (responseCode == BillingClient.BillingResponse.OK) {
                // Handle the success of the consume operation.
                // For example, increase the number of coins inside the user's basket.
                Log.i("tuancoltech3", "IAP onConsumeResponse success, token: " + purchaseToken + ", reload purchases");
                getPurchasedItems();
                //Call consume item API
                if (!TextUtils.isEmpty(mLastConsumeSku)
                        && !TextUtils.isEmpty(mInventoryItemId)) {
                    mPurchasePresenter.consumeItem(mInventoryItemId, 1, Const.API_VERSION);
                }
            } else {
                Log.i("tuancoltech3", "IAP onConsumeResponse, err: " + responseCode);
                //Consume failed, reset last consume item
                mLastConsumeSku = null;
                showError(responseCode);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_test);

        new PurchaseTestPresenter(this,
                Injection.provideUseCaseHandler(),
                Injection.provideGetAllBundle(getApplicationContext()),
                Injection.provideGetMyPurchases(getApplicationContext()),
                Injection.provideValidateOrder(getApplicationContext()),
                Injection.provideConsumeItem(getApplicationContext()));

        initViews();

        initBilling();

        mPurchasePresenter.getAllBundle(Const.API_VERSION);
        mPurchasePresenter.getMyItems(Const.API_VERSION);
    }

    private boolean isAvailableForPurchase(@NonNull String skuId) {
        for (Purchase purchase : mPurchaseList) {
            if (skuId.equals(purchase.getSku())) {
                return false;
            }
        }
        return true;
    }

    private void initViews() {

        mBtnBuySku1 = findViewById(R.id.btn_buy_1);
        mBtnBuySku2 = findViewById(R.id.btn_buy_2);
        findViewById(R.id.btn_consume_1).setOnClickListener(this);
        findViewById(R.id.btn_consume_2).setOnClickListener(this);

//        mBtnBuySku1.setText(getString(R.string.purchase_buy_item_1));
//        mBtnBuySku2.setText(getString(R.string.purchase_buy_item_2));

        mBtnBuySku1.setOnClickListener(this);
        mBtnBuySku2.setOnClickListener(this);

        mRecyclerBundles = findViewById(R.id.recycler_bundles);
        mRecyclerBundles.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerBundles.setLayoutManager(layoutManager);

        mBundleData = new ArrayList<>();
        mAdapterBundle = new BundleAdapter(mBundleData);

        mRecyclerBundles.setAdapter(mAdapterBundle);
        mAdapterBundle.setOnItemClickListener(new BundleAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v,
                                    BundleAdapter.ItemProcessCallback itemProcessCallback) {

                mItemProcessCallback = Preconditions.checkNotNull(itemProcessCallback);
                launchBillingFlow(mBundleData.get(position).getBundleName());
//                if (isAvailableForPurchase(mBundleData.get(position).getBundleName())) {
//
//                    //Purchase the item
//                    launchBillingFlow(mBundleData.get(position).getBundleName());
//                } else {
//
//                    //Consume the item
//                    Purchase purchase = getPurchaseInfo(mBundleData.get(position).getBundleName());
//                    if (purchase != null && mBillingClient != null && mBillingClient.isReady()) {
//                        mBillingClient.consumeAsync(purchase.getPurchaseToken(),
//                                mConsumeResponseListener);
//                        mLastConsumeSku = purchase.getSku();
//                    }
//                }

            }
        });

        mTvEmptyBundles = findViewById(R.id.tv_bundles_empty);
        mTvEmptyInventory = findViewById(R.id.tv_inventory_empty);

        initInventoryView();

        mTextItemQuantity = findViewById(R.id.tv_header_inventory);

    }

    private void initInventoryView() {

        mRecyclerInventory = findViewById(R.id.recycler_inventory);
        mRecyclerInventory.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerInventory.setLayoutManager(layoutManager);

        mDataMyInventory = new ArrayList<>();
        mAdapterInventory = new InventoryAdapter(mDataMyInventory);
        mRecyclerInventory.setAdapter(mAdapterInventory);

        mAdapterInventory.setOnItemClickListener(new InventoryAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v, @NonNull InventoryAdapter
                    .ItemProcessCallback itemProcessCallback) {

                if (mBillingClient != null && mBillingClient.isReady()) {
                    String skuId = mDataMyInventory.get(position);
                    Log.i("tuancoltech1", "consume SKU ID: " + skuId);
                    for (Purchase purchase : mPurchaseList) {
                        Log.i("tuancoltech1", "sku: " + purchase.getSku());
                        if (skuId.equals(purchase.getSku())) {
                            Log.i("tuancoltech3", "IAP start consumeAsync..purchaseToken- " +
                                    " \nsku: " + purchase.getSku() +
                                    " \ntoken: " + purchase.getPurchaseToken());
                            mBillingClient.consumeAsync(purchase.getPurchaseToken(),
                                    mConsumeResponseListener);
                            mLastConsumeSku = purchase.getSku();
                            return;
                        }
                    }
                    Toast.makeText(PurchaseTestActivity.this, "Buy this item first.", Toast.LENGTH_SHORT).show();
                }

//                mPurchasePresenter.consumeItem(mDataMyInventory.get(position),
//                        1, Const.API_VERSION);
            }
        });

        mTvEmptyInventory = findViewById(R.id.tv_inventory_empty);

    }

    private Purchase getPurchaseInfo(@NonNull String skuId) {
        for (Purchase purchase : mPurchaseList) {
            if (skuId.equals(purchase.getSku())) {
                return purchase;
            }
        }
        return null;
    }

    private void initBilling() {

        /**
         * Initiate in app billing connection service
         */
        mBillingClient = BillingClient.newBuilder(this).setListener(this).build();
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int responseCode) {
                if (responseCode == BillingClient.BillingResponse.OK) {
                    // The billing client is ready. You can query purchases here.
                    Log.i("tuancoltech3", "onBillingSetupFinished, loading purchases");
                    /*mPurchaseList = */getPurchasedItems();
//                    Log.i("tuancoltech", "Purchase list size: " + mPurchaseList.size());
//                    for (Purchase purchase : mPurchaseList) {
//                        Log.i("tuancoltech", "sku id: " + purchase.getSku());
//                        switch (purchase.getSku()) {
//
//                            case SKU_ID_1:
//                                break;
//
//                            case SKU_ID_2:
//                                break;
//
//                            default:
//                                break;
//
//
//                        }
//                    }
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                Log.i("tuancoltech", "onBillingServiceDisconnected");
            }
        });

    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        if (responseCode == BillingClient.BillingResponse.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
            Log.i("tuancoltech3", "IAP purchase success, reload purchase list");
            getPurchasedItems();
        } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.
            Log.i("tuancoltech", "onPurchasesUpdated USER_CANCELED");
        } else {
            // Handle any other error codes.
            Log.i("tuancoltech", "onPurchasesUpdated other errors: responseCode = " + responseCode);
            showError(responseCode);
        }
    }

    private void showError(int errorCode) {

        int SERVICE_DISCONNECTED = -1;
        /** Success */
        int OK = 0;
        /** User pressed back or canceled a dialog */
        int USER_CANCELED = 1;
        /** Network connection is down */
        int SERVICE_UNAVAILABLE = 2;
        /** Billing API version is not supported for the type requested */
        int BILLING_UNAVAILABLE = 3;
        /** Requested product is not available for purchase */
        int ITEM_UNAVAILABLE = 4;
        /**
         * Invalid arguments provided to the API. This error can also indicate that the
         * application was
         * not correctly signed or properly set up for In-app Billing in Google Play, or does not
         * have
         * the necessary permissions in its manifest
         */
        int DEVELOPER_ERROR = 5;
        /** Fatal error during the API action */
        int ERROR = 6;
        /** Failure to purchase since item is already owned */
        int ITEM_ALREADY_OWNED = 7;
        /** Failure to consume since item is not owned */
        int ITEM_NOT_OWNED = 8;

        String errorMessage = null;
        switch (errorCode) {

            case BillingClient.BillingResponse.SERVICE_DISCONNECTED:
                errorMessage = "Play Service is not connected.";
                break;

            case BillingClient.BillingResponse.SERVICE_UNAVAILABLE:
                errorMessage = "Network connection is down.";
                break;

            case BillingClient.BillingResponse.BILLING_UNAVAILABLE:
                errorMessage = "Billing API version is not supported for the type requested";
                break;

            case BillingClient.BillingResponse.ITEM_UNAVAILABLE:
                errorMessage = "Requested product is not available for purchase";
                break;

            case BillingClient.BillingResponse.DEVELOPER_ERROR:
                errorMessage = "Invalid arguments provided to the API";
                break;

            case BillingClient.BillingResponse.ERROR:
                errorMessage = "Fatal error during the API action";
                break;

            case BillingClient.BillingResponse.ITEM_ALREADY_OWNED:
                errorMessage = "Item already owned. Consume it first.";
                break;

            case BillingClient.BillingResponse.ITEM_NOT_OWNED:
                errorMessage = "Failure to consume since item is not owned.";
                break;

            default:
                break;

        }

        if (!TextUtils.isEmpty(errorMessage)) {
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    private void handlePurchase(@Nullable Purchase purchase) {
        Log.i("tuancoltech3", "handlePurchase: " + purchase.getPurchaseToken() + ", " + purchase
                .getSku() + ", " + purchase.getSignature());
        mPurchasePresenter.validateOrder(purchase.getPurchaseToken(),
                purchase.getSku(),
                purchase.getSignature(),
                Const.API_VERSION);
    }

    /**
     * Start an in app purchase for item with skuId
     *
     * @param skuId item's skuId
     */
    private void launchBillingFlow(@NonNull String skuId) {
        Log.i("tuancoltech", "launchBillingFlow, sku: " + skuId);

        if (mBillingClient.isReady()) {
            BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                    .setSku(skuId)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();
            int responseCode = mBillingClient.launchBillingFlow(this, flowParams);
            if (responseCode != BillingClient.BillingResponse.OK) {
                Log.i("tuancoltech3", "IAP launch billing flow fail");
                showError(responseCode);
            }
        }
    }

    /**
     * Query for purchased items
     */
    private List<Purchase> getPurchasedItems() {
        if (mBillingClient.isReady()) {
//            Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases
//                    (BillingClient.SkuType.INAPP);
//            if (purchasesResult != null) {
//                Log.i("tuancoltech", "some items exist..");
//                return purchasesResult.getPurchasesList();
//            } else {
//                Log.i("tuancoltech", "no purchased items..");
//                return null;
//            }

//            mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP,
//                    new PurchaseHistoryResponseListener() {
//                        @Override
//                        public void onPurchaseHistoryResponse(@BillingClient.BillingResponse int responseCode,
//                                                              List<Purchase> purchasesList) {
//                            if (responseCode == BillingClient.BillingResponse.OK
//                                    && purchasesList != null) {
//
//                                mPurchaseList = purchasesList;
//                                Log.i("tuancoltech3", "IAP Purchase list size: " + mPurchaseList.size());
//                                for (Purchase purchase : mPurchaseList) {
//                                    Log.i("tuancoltech3", "IAP sku id: " + purchase.getSku());
//                                    switch (purchase.getSku()) {
//
//                                        case SKU_ID_1:
//                                            break;
//
//                                        case SKU_ID_2:
//                                            break;
//
//                                        default:
//                                            break;
//
//
//                                    }
//                                }
//                            }
//                        }
//                    });
            Purchase.PurchasesResult purchases = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
            mPurchaseList = purchases.getPurchasesList();
            Log.i("tuancoltech3", "IAP purchased list size: " + mPurchaseList.size());
            for (Purchase purchase : mPurchaseList) {
                Log.i("tuancoltech3", "IAP purchased sku: " + purchase.getSku());
            }


        } else {
            Log.i("tuancoltech", "billing client not ready..");
        }
        return null;
    }

    @Override
    public void setLoading(boolean isActive) {
        setLoadingDialog(isActive);
    }

    @Override
    public void showGetAllBundleSuccess(@NonNull GetAllBundleResponse getAllBundleResponse) {
        setLoading(false);
        mBundleData = getAllBundleResponse.getBundleData();
        if (mBundleData != null && mBundleData.size() > 0) {
            mRecyclerBundles.setVisibility(View.VISIBLE);
            mTvEmptyBundles.setVisibility(View.GONE);
            mAdapterBundle.replaceData(mBundleData);
        } else {
            mRecyclerBundles.setVisibility(View.GONE);
            mTvEmptyBundles.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showGetAllBundleFail(String errorCode) {
        setLoading(false);
    }

    @Override
    public void showGetMyItemSuccess(@NonNull GetMyPurchaseResponse getMyPurchaseResponse) {
        setLoading(false);
        mDataMyInventory = getMyPurchaseResponse.getBundleNames();
        if (mDataMyInventory != null && mDataMyInventory.size() > 0) {

            mRecyclerInventory.setVisibility(View.VISIBLE);
            mTvEmptyInventory.setVisibility(View.GONE);
            mAdapterInventory.replaceData(mDataMyInventory);
        } else {

            mRecyclerInventory.setVisibility(View.GONE);
            mTvEmptyInventory.setVisibility(View.VISIBLE);
        }

        List<UserItemModel> items = getMyPurchaseResponse.getUserItems();
        if (items != null && items.size() > 0) {
            mTextItemQuantity.setText(String.format(getString(R.string.purchase_inventory_header),
                    items.get(0).getQuantity()));
            mInventoryItemId = items.get(0).getId();
        }

    }

    @Override
    public void showGetMyItemFail(String errorCode) {
        setLoading(false);
        Toast.makeText(this, "Get my items failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showValidateOrderSuccess(@NonNull ValidateOrderResponse validateOrderResponse) {
        setLoading(false);
        Toast.makeText(this, "Validate successfully !", Toast.LENGTH_SHORT).show();
        mItemProcessCallback.onItemProcessed(true);
        mPurchasePresenter.getMyItems(Const.API_VERSION);
    }

    @Override
    public void showValidateOrderFail(String errorCode) {
        setLoading(false);
        Toast.makeText(this, "Error: " + errorCode, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showConsumeItemSuccess(@NonNull ConsumeItemResponse consumeItemResponse) {
        setLoading(false);
        Toast.makeText(this, "Consume item successfully", Toast.LENGTH_SHORT).show();
//        mItemProcessCallback.onItemProcessed(false);
        mPurchasePresenter.getMyItems(Const.API_VERSION);
    }

    @Override
    public void showConsumeItemFail(String errorCode) {
        setLoading(false);
        Toast.makeText(this, "Consume item failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(PurchaseTestContract.Presenter presenter) {
        mPurchasePresenter = Preconditions.checkNotNull(presenter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_buy_1:
                launchBillingFlow(SKU_ID_1);
                break;


            case R.id.btn_buy_2:
                launchBillingFlow(SKU_ID_2);
                break;

            case R.id.btn_consume_1:
                if (mBillingClient != null && mBillingClient.isReady()) {
                    Log.i("tuancoltech1", "consume sku 1, SKU ID: " + SKU_ID_1);
                    for (Purchase purchase : mPurchaseList) {
                        Log.i("tuancoltech1", "sku: " + purchase.getSku());
                        if (SKU_ID_1.equals(purchase.getSku())) {
                            Log.i("tuancoltech", "start consumeAsync..");
                            mBillingClient.consumeAsync(purchase.getPurchaseToken(),
                                    mConsumeResponseListener);
                            mLastConsumeSku = purchase.getSku();
                        }
                    }
                }
                break;

            case R.id.btn_consume_2:
                if (mBillingClient != null && mBillingClient.isReady()) {
                    Log.i("tuancoltech", "consume sku 2");
                    for (Purchase purchase : mPurchaseList) {
                        if (SKU_ID_2.equals(purchase.getSku())) {
                            mBillingClient.consumeAsync(purchase.getPurchaseToken(),
                                    mConsumeResponseListener);
                            mLastConsumeSku = purchase.getSku();
                        }
                    }
                }
                break;

            default:
                break;
        }
    }
}
