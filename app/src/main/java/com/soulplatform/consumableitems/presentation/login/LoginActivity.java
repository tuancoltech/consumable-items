package com.soulplatform.consumableitems.presentation.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.soulplatform.consumableitems.R;
import com.soulplatform.consumableitems.domain.model.authentication.login.LoginResponseModel;
import com.soulplatform.consumableitems.mvpcore.Injection;
import com.soulplatform.consumableitems.presentation.common.BaseActivity;
import com.soulplatform.consumableitems.presentation.purchase.PurchaseTestActivity;
import com.soulplatform.consumableitems.utils.Const;
import com.soulplatform.consumableitems.utils.Preconditions;

import java.io.IOException;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/14/2017.
 */

public class LoginActivity extends BaseActivity implements LoginContract.View,
        View.OnClickListener {

    private LoginContract.Presenter mLoginPresenter;

    private EditText mEdtUserName;
    private EditText mEdtPassword;
    private Button mBtnLogin;

    private final String API_KEY =
            /*"b591130e1113d2d508a846cdecbabec3"*/"aae3bed1a69445f320ef1c37e7c2bfbe";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        new LoginPresenter(this,
                Injection.provideUseCaseHandler(),
                Injection.provideLogin(getApplicationContext()));

        initViews();
    }

    private void initViews() {
        mEdtUserName = findViewById(R.id.edit_text_username);
        mEdtPassword = findViewById(R.id.edit_text_password);
        mBtnLogin = findViewById(R.id.button_login);

        mBtnLogin.setOnClickListener(this);
    }

    @Override
    public void setLoading(boolean isActive) {
        setLoadingDialog(isActive);
    }

    @Override
    public void showLoginSuccess(@NonNull LoginResponseModel loginResponseModel) {
        setLoading(false);
        Toast.makeText(this, "Login successfully !", Toast.LENGTH_SHORT).show();

        Intent purchaseIntent = new Intent(this, PurchaseTestActivity.class);
        purchaseIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(purchaseIntent);
        finish();
    }

    @Override
    public void showLoginError(String errorCode) {
        setLoading(false);
        Toast.makeText(this, "Login failed. Error code: " + errorCode, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        mLoginPresenter = Preconditions.checkNotNull(presenter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.button_login:
                mLoginPresenter.login(mEdtUserName.getText().toString(),
                        mEdtPassword.getText().toString(),
                        API_KEY,
                        Const.API_VERSION);
                break;

            default:
                break;

        }
    }
}
