package com.soulplatform.consumableitems.presentation.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.soulplatform.consumableitems.R;

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;

    protected void setupActionbar(Toolbar toolbar, String title, boolean homeEnabled, int
            homeResId) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(homeEnabled);
            actionBar.setTitle(title);
            actionBar.setHomeAsUpIndicator(homeResId);
        }
    }

    private void showProgressDialog() {

        try {

            if (mProgressDialog == null) {

                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setIndeterminate(true);

                mProgressDialog.show();

                mProgressDialog.setCancelable(false);

                if (mProgressDialog.getWindow() != null) {
                    mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android
                            .graphics.Color.TRANSPARENT));
                }

                mProgressDialog.setContentView(R.layout.dialog_loading);

            } else if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {

        if (mProgressDialog != null && mProgressDialog.isShowing()) {

            try {
                mProgressDialog.dismiss();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } finally {
                mProgressDialog = null;
            }

        }
    }

    public void setLoadingDialog(final boolean isActive) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isActive) {
                    showProgressDialog();
                } else {
                    hideProgressDialog();
                }
            }
        });

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context
                            .INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {

            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }
}
