package com.soulplatform.consumableitems.presentation.purchase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.soulplatform.consumableitems.R;

import java.util.List;

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.ViewHolder> {

    private MyClickListener mClickListener;

    private List<String> mDataInventory;

    private Context mContext;

    public InventoryAdapter(List<String> dataInventory) {
        mDataInventory = dataInventory;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String String = mDataInventory.get(position);

        holder.mTvItemName.setText(mDataInventory.get(position));
        holder.mBtnConsume.setText(mContext.getString(R.string.consume));
        holder.mBtnConsume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onItemClick(position, view, new ItemProcessCallback() {
                    @Override
                    public void onItemProcessed(boolean isPurchasing) {
                        holder.mBtnConsume.setEnabled(false);
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mDataInventory != null)
            return mDataInventory.size();
        else
            return 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bundle,
                parent, false);

        return new ViewHolder(view);
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.mClickListener = myClickListener;
    }

    public void replaceData(List<String> commentData) {
        mDataInventory = commentData;
        notifyDataSetChanged();
    }

    public List<String> getData() {
        return this.mDataInventory;
    }

    public interface MyClickListener {

        void onItemClick(int position, View v, @NonNull ItemProcessCallback itemProcessCallback);

    }

    public interface ItemProcessCallback {

        void onItemProcessed(boolean isPurchasing);

    }

    public

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTvItemName;
        private final Button mBtnConsume;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvItemName = itemView.findViewById(R.id.tv_bundle_name);
            mBtnConsume = itemView.findViewById(R.id.btn_buy);
        }

    }
}
