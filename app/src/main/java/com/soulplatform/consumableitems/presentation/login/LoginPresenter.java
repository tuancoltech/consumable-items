package com.soulplatform.consumableitems.presentation.login;

import android.support.annotation.NonNull;
import android.util.Log;

import com.soulplatform.consumableitems.domain.usecase.authentication.Login;
import com.soulplatform.consumableitems.mvpcore.UseCase;
import com.soulplatform.consumableitems.mvpcore.UseCaseHandler;
import com.soulplatform.consumableitems.utils.MyPreferences;
import com.soulplatform.consumableitems.utils.Preconditions;

import java.io.IOException;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/16/2017.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View mView;

    private final UseCaseHandler mUseCaseHandler;

    private final Login mLogin;

    public LoginPresenter(@NonNull LoginContract.View view,
                          @NonNull UseCaseHandler useCaseHandler,
                          @NonNull Login login) {

        mView = Preconditions.checkNotNull(view);
        mUseCaseHandler = Preconditions.checkNotNull(useCaseHandler);
        mLogin = Preconditions.checkNotNull(login);
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void login(@NonNull String userName, @NonNull String password, @NonNull String apiKey,
                      @NonNull String apiVersion) {

        mView.setLoading(true);
        mUseCaseHandler.execute(mLogin, new Login.RequestValue(userName,
                        password, apiKey, apiVersion),
                new UseCase.UseCaseCallback<Login.ResponseValue>() {
                    @Override
                    public void onSuccess(Login.ResponseValue response) {
                        mView.showLoginSuccess(response.getLoginResponseModel());
                        MyPreferences.saveSessionToken(response.getLoginResponseModel()
                                .getAuthorizationModel().getSessionToken());
                        MyPreferences.saveUserId(response.getLoginResponseModel().getMeModel()
                                .getId());

                        Log.i("tuancoltech", "serverTime: " +
                                response.getLoginResponseModel().getAdditionalInfoModel()
                                        .getServerTime());
                    }

                    @Override
                    public void onError(String errorCode) {
                        Log.i("tuancoltech", " response error msg 4: " + errorCode);
                        mView.showLoginError(errorCode);
                    }
                });
    }
}
