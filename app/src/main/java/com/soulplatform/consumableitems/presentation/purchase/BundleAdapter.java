package com.soulplatform.consumableitems.presentation.purchase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.soulplatform.consumableitems.R;
import com.soulplatform.consumableitems.domain.model.purchase.bundle.BundleModel;

import java.util.List;

public class BundleAdapter extends RecyclerView.Adapter<BundleAdapter.ViewHolder> {

    private MyClickListener mClickListener;

    private List<BundleModel> mBundleData;

    private Context mContext;

    public BundleAdapter(List<BundleModel> bundleData) {
        mBundleData = bundleData;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final BundleModel bundleModel = mBundleData.get(position);

        holder.mTvBundleName.setText(bundleModel.getBundleName());
        holder.mBtnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onItemClick(position, view, new ItemProcessCallback() {
                    @Override
                    public void onItemProcessed(boolean isPurchasing) {
//                        holder.mBtnBuy.setText(isPurchasing ? "Consume" : "Buy");
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mBundleData != null)
            return mBundleData.size();
        else
            return 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bundle,
                parent, false);

        return new ViewHolder(view);
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.mClickListener = myClickListener;
    }

    public void replaceData(List<BundleModel> commentData) {
        mBundleData = commentData;
        notifyDataSetChanged();
    }

    public List<BundleModel> getData() {
        return this.mBundleData;
    }

    public interface MyClickListener {

        void onItemClick(int position, View v, @NonNull ItemProcessCallback itemProcessCallback);

    }

    public interface ItemProcessCallback {

        void onItemProcessed(boolean isPurchasing);

    }

    public

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTvBundleName;
        private final Button mBtnBuy;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvBundleName = itemView.findViewById(R.id.tv_bundle_name);
            mBtnBuy = itemView.findViewById(R.id.btn_buy);
        }

    }
}
