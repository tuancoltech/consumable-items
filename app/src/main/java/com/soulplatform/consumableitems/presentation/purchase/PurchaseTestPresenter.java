package com.soulplatform.consumableitems.presentation.purchase;

import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.domain.usecase.purchase.ConsumeItem;
import com.soulplatform.consumableitems.domain.usecase.purchase.GetAllBundle;
import com.soulplatform.consumableitems.domain.usecase.purchase.GetMyPurchases;
import com.soulplatform.consumableitems.domain.usecase.purchase.ValidateOrder;
import com.soulplatform.consumableitems.mvpcore.UseCase;
import com.soulplatform.consumableitems.mvpcore.UseCaseHandler;
import com.soulplatform.consumableitems.utils.Preconditions;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Tuan Nguyen on 12/17/2017.
 */

public class PurchaseTestPresenter implements PurchaseTestContract.Presenter {

    private final PurchaseTestContract.View mView;
    private final UseCaseHandler mUseCaseHandler;
    private final GetAllBundle mGetAllBundle;
    private final GetMyPurchases mGetMyPurchases;
    private final ValidateOrder mValidateOrder;
    private final ConsumeItem mConsumeItem;

    public PurchaseTestPresenter(@NonNull PurchaseTestContract.View view,
                                 @NonNull UseCaseHandler useCaseHandler,
                                 @NonNull GetAllBundle getAllBundle,
                                 @NonNull GetMyPurchases getMyPurchases,
                                 @NonNull ValidateOrder validateOrder,
                                 @NonNull ConsumeItem consumeItem) {

        mView = Preconditions.checkNotNull(view);
        mUseCaseHandler = Preconditions.checkNotNull(useCaseHandler);
        mGetAllBundle = Preconditions.checkNotNull(getAllBundle);
        mGetMyPurchases = Preconditions.checkNotNull(getMyPurchases);
        mValidateOrder = Preconditions.checkNotNull(validateOrder);
        mConsumeItem = Preconditions.checkNotNull(consumeItem);

        mView.setPresenter(this);
    }

    @Override
    public void getAllBundle(@NonNull String apiVersion) {
        mView.setLoading(true);
        mUseCaseHandler.execute(mGetAllBundle, new GetAllBundle.RequestValue(apiVersion),
                new UseCase.UseCaseCallback<GetAllBundle.ResponseValue>() {
                    @Override
                    public void onSuccess(GetAllBundle.ResponseValue response) {
                        mView.showGetAllBundleSuccess(response.getGetAllBundleResponse());
                    }

                    @Override
                    public void onError(String errorCode) {
                        mView.showGetAllBundleFail(errorCode);
                    }
                });
    }

    @Override
    public void getMyItems(@NonNull String apiVersion) {
        mView.setLoading(true);
        mUseCaseHandler.execute(mGetMyPurchases, new GetMyPurchases.RequestValue(apiVersion),
                new UseCase.UseCaseCallback<GetMyPurchases.ResponseValue>() {
                    @Override
                    public void onSuccess(GetMyPurchases.ResponseValue response) {
                        mView.showGetMyItemSuccess(response.getGetMyPurchaseResponse());
                    }

                    @Override
                    public void onError(String errorCode) {
                        mView.showGetMyItemFail(errorCode);
                    }
                });
    }

    @Override
    public void validateOrder(@NonNull String purchaseToken,
                              @NonNull String skuId, @NonNull String signature,
                              @NonNull String apiVersion) {
        mView.setLoading(true);
        mUseCaseHandler.execute(mValidateOrder, new ValidateOrder.RequestValue(purchaseToken,
                        skuId, signature, apiVersion),
                new UseCase.UseCaseCallback<ValidateOrder.ResponseValue>() {
                    @Override
                    public void onSuccess(ValidateOrder.ResponseValue response) {
                        mView.showValidateOrderSuccess(response.getValidateOrderResponse());
                    }

                    @Override
                    public void onError(String errorCode) {
                        mView.showValidateOrderFail(errorCode);
                    }
                });
    }

    @Override
    public void consumeItem(@NonNull String skuId,
                            @NonNull int quantity,
                            @NonNull String apiVersion) {

        mView.setLoading(true);
        mUseCaseHandler.execute(mConsumeItem,
                new ConsumeItem.RequestValue(skuId, quantity, apiVersion),
                new UseCase.UseCaseCallback<ConsumeItem.ResponseValue>() {
                    @Override
                    public void onSuccess(ConsumeItem.ResponseValue response) {
                        mView.showConsumeItemSuccess(response.getConsumeItemResponse());
                    }

                    @Override
                    public void onError(String errorCode) {
                        mView.showConsumeItemFail(errorCode);
                    }
                });

    }

    @Override
    public void start() {

    }
}
