/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.soulplatform.consumableitems.mvpcore;

import android.content.Context;
import android.support.annotation.NonNull;

import com.soulplatform.consumableitems.data.repository.authentication.source.AuthenRepository;
import com.soulplatform.consumableitems.data.repository.authentication.source.local
        .AuthenLocalDataSource;
import com.soulplatform.consumableitems.data.repository.authentication.source.remote
        .AuthenRemoteDataSource;
import com.soulplatform.consumableitems.data.repository.purchase.source.PurchaseRepository;
import com.soulplatform.consumableitems.data.repository.purchase.source.local
        .PurchaseLocalDataSource;
import com.soulplatform.consumableitems.data.repository.purchase.source.remote
        .PurchaseRemoteDataSource;
import com.soulplatform.consumableitems.domain.usecase.authentication.Login;
import com.soulplatform.consumableitems.domain.usecase.purchase.ConsumeItem;
import com.soulplatform.consumableitems.domain.usecase.purchase.GetAllBundle;
import com.soulplatform.consumableitems.domain.usecase.purchase.GetMyPurchases;
import com.soulplatform.consumableitems.domain.usecase.purchase.ValidateOrder;

/**
 * Enables injection of mock implementations for some data sources. This is useful for
 * testing, since it allows us to use
 * a fake instance of the class to isolate the dependencies and run a test hermetically.
 */
public class Injection {

    /**
     * Provide an access point to the UseCaseHandler
     *
     * @return a UseCaseHandler instance
     */
    public static UseCaseHandler provideUseCaseHandler() {
        return UseCaseHandler.getInstance();
    }

    private static AuthenRepository provideAuthenRepository(@NonNull Context context) {
        return AuthenRepository.getInstance(AuthenLocalDataSource.getInstance(context),
                AuthenRemoteDataSource.getInstance(context));
    }

    private static PurchaseRepository providePurchaseRepository(@NonNull Context context) {
        return PurchaseRepository.getInstance(PurchaseLocalDataSource.getInstance(context),
                PurchaseRemoteDataSource.getInstance(context));
    }

    public static Login provideLogin(@NonNull Context context) {
        return new Login(provideAuthenRepository(context));
    }

    public static GetAllBundle provideGetAllBundle(@NonNull Context context) {
        return new GetAllBundle(providePurchaseRepository(context));
    }

    public static ConsumeItem provideConsumeItem(@NonNull Context context) {
        return new ConsumeItem(providePurchaseRepository(context));
    }

    public static ValidateOrder provideValidateOrder(@NonNull Context context) {
        return new ValidateOrder(providePurchaseRepository(context));
    }

    public static GetMyPurchases provideGetMyPurchases(@NonNull Context context) {
        return new GetMyPurchases(providePurchaseRepository(context));
    }

}
